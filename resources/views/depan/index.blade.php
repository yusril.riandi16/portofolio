<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Yusril Riandi</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('depan') }}/img/favicon.png" rel="icon">
  <link href="{{ asset('depan') }}/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('depan') }}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ asset('depan') }}/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{ asset('depan') }}/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="{{ asset('depan') }}/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/devicons/devicon@v2.15.1/devicon.min.css">

  <!-- Template Main CSS File -->
  <link href="{{ asset('depan') }}/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: DevFolio - v4.9.1
  * Template URL: https://bootstrapmade.com/devfolio-bootstrap-portfolio-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center justify-content-between">

      <h1 class="logo"><a href="index.html">Manajemen Informatika</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">About</a></li>
            <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#experience">Experience</a></li>
            <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#education">Education</a></li>
            <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#skills">Skills</a></li>
            <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#interests">Interests</a></li>
            <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#awards">Awards</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <div id="hero" class="hero route bg-image" style="background-image: url({{ asset('depan') }}/img/CC.jpg)">
    <div class="overlay-itro"></div>
    <div class="hero-content display-table">
      <div class="table-cell">
        <div class="container">
          <!--<p class="display-6 color-d">Hello, world!</p>-->
          <h1 class="hero-title mb-4">{{ $about->judul }}</h1>
          <p class="hero-subtitle"><span class="typed" data-typed-items="Designer, Mobile Developer"></span></p>
          <!-- <p class="pt-3"><a class="btn btn-primary btn js-scroll px-4" href="#about" role="button">Learn More</a></p> -->
        </div>
      </div>
    </div>
  </div><!-- End Hero Section -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about-mf sect-pt4 route">
      <div class="container">
        <div class="row">

          <div class="title-box">
            <h3 class="title-a text-center">
              About
            </h3>
            <p class="subtitle-a text-center">
              Sedikit tentang saya yang dapat saya ditampilkan.
            </p>
            <div class="line-mf"></div>
            <br><br>
           

          <div class="col-sm-12">
            <div class="box-shadow-full">
              <div class="row">
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-sm-6 col-md-5">
                      <div class="about-img">
                        <img src="{{ asset('foto'). "/" .get_meta_value('_foto') }}" class="img-fluid img-profile rounded-circle mx-auto mb-2" alt="">
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-7">
                      <div class="about-info">
                        <br>
                        <p><span class="title-s">Nama: </span> <span>{!! set_about_nama($about->judul) !!}</span></p>
                        <p><span class="title-s">Kota: </span> <span>{{get_meta_value('_kota')}}</span></p>
                        <p><span class="title-s">Provinsi: </span> <span>{{get_meta_value('_provinsi')}}</span></p>
                        <p><span class="title-s">Phone: </span> <span>{{get_meta_value('_nohp')}}</span></p>
                        <p><span class="title-s">Email: </span> <span>{{get_meta_value('_email')}}</span></p>
                      </div>
                    </div>
                  </div>
                  
                  <div class="socials">
                    <ul>
                      <li><a href="{{ get_meta_value('_facebook') }}"><span class="ico-circle"><i class="bi bi-facebook"></i></span></a></li>
                      <li><a href="{{ get_meta_value('_github') }}"><span class="ico-circle"><i class="bi bi-github"></i></span></a></li>
                      <li><a href="{{ get_meta_value('_twitter') }}"><span class="ico-circle"><i class="bi bi-twitter"></i></span></a></li>
                      <li><a href="{{ get_meta_value('_linkedin') }}"><span class="ico-circle"><i class="bi bi-linkedin"></i></span></a></li>
                    </ul>
                  </div>
                  
                </div>
                <div class="col-md-6">
                  <div class="about-me pt-4 pt-md-0">
                    <div class="title-box-2">
                      <h5 class="title-left">
                        About
                      </h5>
                    </div>
                    <p class="lead">
                      {!! $about->isi !!}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End About Section -->

    <!-- ======= Services Section ======= -->

    <section class="resume-section" id="experience">
      <div class="container">
        

          <div class="title-box">
            <h3 class="title-a text-center">
              Experience
            </h3>
            <p class="subtitle-a text-center">
              Sedikit tentang pengalaman saya yang dapat saya ditampilkan.
            </p>
            <div class="line-mf"></div>
            <br><br>
           
            <div class="box-shadow-full">
                      
                         <div class="resume-section-content">
                        <h2 class="mb-5">Experience</h2>
                        <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                            @foreach ($experience as $item)
                            <div class="flex-grow-1">
                                <h3 class="mb-0">{{$item->judul}}</h3>
                                <div class="subheading mb-3">{{$item->info1}}</div>
                                {!! $item->isi !!}
                            </div>
                            <div class="flex-shrink-0"><span class="text-primary">{{$item->tgl_mulai_indo}} - {{$item->tgl_akhir_indo}}</span></div>
                        </div>
                        
                            @endforeach
                        
              </div>
      </div>
    </section><!-- End Services Section -->



    <!-- ======= Counter Section ======= -->

    <section class="resume-section" id="education">
      <div class="container">
        

          <div class="title-box">
            <h3 class="title-a text-center">
              Education
            </h3>
            <p class="subtitle-a text-center">
              Sedikit tentang pendidikan saya yang dapat saya ditampilkan.
            </p>
            <div class="line-mf"></div>
            <br><br>
           
            <div class="box-shadow-full">
                      
              <div class="resume-section-content">
                <h2 class="mb-5">Education</h2>
                @foreach ($education as $item)

                <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                    <div class="flex-grow-1">
                        <h3 class="mb-0">{{$item->judul}}</h3>
                        <div class="subheading mb-3">{{$item->info1}}</div>
                        <div>{{$item->info2}}</div>
                        <p>{{$item->info3}}</p>
                    </div>
                    <div class="flex-shrink-0"><span class="text-primary">{{$item->tgl_mulai_indo}} - {{$item->tgl_akhir_indo}}</span></div>
                </div>

                @endforeach
              
            </div>
      </div>
    </section><!-- End Counter Section -->


    <!-- ======= Portfolio Section ======= -->

    <section class="resume-section" id="skills">
      <div class="container">
        

          <div class="title-box">
            <h3 class="title-a text-center">
              Skill
            </h3>
            <p class="subtitle-a text-center">
              Sedikit tentang skill saya yang dapat saya ditampilkan.
            </p>
            <div class="line-mf"></div>
            <br><br>
           
            <div class="box-shadow-full">
                      
              <div class="resume-section-content">
                <h2 class="mb-5">Skills</h2>
                <div class="subheading mb-3">Programming Languages & Tools</div>
                <ul class="list-inline dev-icons">
                    @foreach (explode(', ', get_meta_value('_language')) as $item)

                    <li class="list-inline-item"><i class="devicon-{{strtolower($item)}}-plain"></i></li>
                        
                    @endforeach
                
                    
                </ul>
                <div class="subheading mb-3">Workflow</div>
                {!! set_list_workflow(get_meta_value('_workflow')) !!}
                
            </div>
      </div>
    </section><!-- End Portfolio Section -->

    <!-- ======= Testimonials Section ======= -->

    <section class="resume-section" id="interests">
      <div class="container">
        

          <div class="title-box">
            <h3 class="title-a text-center">
              Interest
            </h3>
            <p class="subtitle-a text-center">
              Sedikit tentang interest saya yang dapat saya ditampilkan.
            </p>
            <div class="line-mf"></div>
            <br><br>
           
            <div class="box-shadow-full">
                      
              <div class="resume-section-content">
                <h2 class="mb-5">{{ $interest->judul }}</h2>
               {!! $interest->isi !!}
            </div>
            </div>
        </section><!-- End testimonial item -->
            

    <!-- ======= Blog Section ======= -->

    <section class="resume-section" id="awards">
      <div class="container">
        

          <div class="title-box">
            <h3 class="title-a text-center">
              Awards
            </h3>
            <p class="subtitle-a text-center">
              Sedikit tentang awards saya yang dapat saya ditampilkan.
            </p>
            <div class="line-mf"></div>
            <br><br>
           
            <div class="box-shadow-full">
                      
              <div class="resume-section-content">
                <h2 class="mb-5">{{ $award->judul }}</h2>

                    {!! set_list_award($award->isi) !!}
            </div>
            </div>
        </section><!-- End Blog Section -->

    <!-- ======= Contact Section ======= -->
    
    <!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="copyright-box">
            <p class="copyright">&copy; Copyright <strong>DevFolio</strong>. All Rights Reserved</p>
            <div class="credits">
              <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=DevFolio
            -->
              Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ asset('depan') }}/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="{{ asset('depan') }}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('depan') }}/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="{{ asset('depan') }}/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="{{ asset('depan') }}/vendor/typed.js/typed.min.js"></script>
  <script src="{{ asset('depan') }}/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('depan') }}/js/main.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

</body>

</html>